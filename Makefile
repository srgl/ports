PORTSDIR = /usr/ports
PORTS = devel/qbe x11-wm/dwm x11/slstatus

install:
.for PORT in ${PORTS}
	cp -R ${PORT} ${PORTSDIR}/${PORT:H}
.endfor

uninstall:
.for PORT in ${PORTS}
	rm -fr ${PORTSDIR}/${PORT}
.endfor
